from bson.objectid import ObjectId
from pydantic import BaseModel
from bson import errors
from jwtdown_fastapi.authentication import Token


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        if value:
            try:
                ObjectId(value)
            except errors.InvalidId:
                raise ValueError(f"Not a valid object id: {value}")
        return value


class AccountForm(BaseModel):
    username: str
    password: str


class AccountIn(BaseModel):
    username: str
    password: str
    full_name: str
    email: str


class AccountOut(BaseModel):
    id: str
    username: str
    full_name: str


class Account(AccountOut):
    hashed_password: str


class AccountToken(Token):
    account: AccountOut
