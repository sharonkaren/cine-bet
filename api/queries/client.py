from pymongo import MongoClient
import os

DB_URL = os.environ.get("DB_URL")
DB_NAME = os.environ.get("DB_NAME")

client = MongoClient(DB_URL)
db = client[DB_NAME]


class MovieQueries:
    @property
    def collection(self):
        return db[self.collection_name]
